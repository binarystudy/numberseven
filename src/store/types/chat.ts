import { Message } from '../../types';

export interface ChatState {
  messages: Message[];
  editModal: boolean;
  preloader: boolean;
  message: Message | null;
}

export enum ChatActionsTypes {
  MESSAGE_FOR_EDIT = 'MESSAGE_FOR_EDIT',
  EDIT_MESSAGE = 'EDIT_MESSAGE',
  ADD_MESSAGE = 'ADD_MESSAGE',
  DELETE_MESSAGE = 'DELETE_MESSAGE',
  ADD_MESSAGES = 'ADD_MESSAGES',
  SET_PRELOADER = 'SET_PRELOADER',
  SET_EDIT_MODAL = 'SET_EDIT_MODAL',
  SET_LIKE = 'SET_LIKE',
}

interface setMessageForEditAction {
  type: ChatActionsTypes.MESSAGE_FOR_EDIT;
  payload: null | Message;
}

interface EditMessageAction {
  type: ChatActionsTypes.EDIT_MESSAGE;
  payload: Message;
}

interface AddMessageAction {
  type: ChatActionsTypes.ADD_MESSAGE;
  payload: Message;
}

interface AddMessagesAction {
  type: ChatActionsTypes.ADD_MESSAGES;
  payload: Message[];
}

interface DeleteMessageAction {
  type: ChatActionsTypes.DELETE_MESSAGE;
  payload: string;
}

interface SetPreloaderAction {
  type: ChatActionsTypes.SET_PRELOADER;
  payload: boolean;
}

interface SetEditModalAction {
  type: ChatActionsTypes.SET_EDIT_MODAL;
  payload: boolean;
}

interface SetLikeAction {
  type: ChatActionsTypes.SET_LIKE;
  payload: string;
}

export type ChatActions =
  | AddMessageAction
  | AddMessagesAction
  | DeleteMessageAction
  | SetPreloaderAction
  | SetEditModalAction
  | EditMessageAction
  | SetLikeAction
  | setMessageForEditAction;

import { ChatState, ChatActions, ChatActionsTypes } from '../types';

const initialize: ChatState = {
  messages: [],
  editModal: false,
  preloader: true,
  message: null,
};

export const chatReducers = (state = initialize, action: ChatActions): ChatState => {
  const { messages } = state;
  switch (action.type) {
    case ChatActionsTypes.MESSAGE_FOR_EDIT:
      return {
        ...state,
        message: action.payload,
      };
    case ChatActionsTypes.EDIT_MESSAGE:
      return {
        ...state,
        messages: messages.map((message) => {
          if (message.id === action.payload.id) {
            return action.payload;
          }
          return message;
        }),
      };
    case ChatActionsTypes.ADD_MESSAGE:
      return {
        ...state,
        messages: [...messages, action.payload],
      };
    case ChatActionsTypes.ADD_MESSAGES:
      return {
        ...state,
        messages: action.payload,
      };
    case ChatActionsTypes.DELETE_MESSAGE:
      return {
        ...state,
        messages: messages.filter((message) => message.id !== action.payload),
      };
    case ChatActionsTypes.SET_EDIT_MODAL:
      return {
        ...state,
        editModal: action.payload,
      };
    case ChatActionsTypes.SET_PRELOADER:
      return {
        ...state,
        preloader: action.payload,
      };
    case ChatActionsTypes.SET_LIKE:
      return {
        ...state,
        messages: messages.map((message) => {
          if (message.id !== action.payload) {
            return message;
          }
          return {
            ...message,
            liked: !message?.liked,
          };
        }),
      };

    default:
      return state;
  }
};

import { combineReducers } from 'redux';
import { chatReducers } from './chat';

export const rootReducer = combineReducers({
  chat: chatReducers,
});

export default rootReducer;

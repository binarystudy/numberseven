import { Message } from '../../types';
import { ChatActions, ChatActionsTypes } from '../types';

export const setMessageForEdit = (payload: null | Message): ChatActions => ({
  type: ChatActionsTypes.MESSAGE_FOR_EDIT,
  payload,
});

export const editMessageAction = (payload: Message): ChatActions => ({
  type: ChatActionsTypes.EDIT_MESSAGE,
  payload,
});

export const addMessageAction = (payload: Message): ChatActions => ({
  type: ChatActionsTypes.ADD_MESSAGE,
  payload,
});

export const addMessagesAction = (payload: Message[]): ChatActions => ({
  type: ChatActionsTypes.ADD_MESSAGES,
  payload,
});

export const deleteMessageAction = (payload: string): ChatActions => ({
  type: ChatActionsTypes.DELETE_MESSAGE,
  payload,
});

export const setPreloaderAction = (payload: boolean): ChatActions => ({
  type: ChatActionsTypes.SET_PRELOADER,
  payload,
});

export const setEditModalAction = (payload: boolean): ChatActions => ({
  type: ChatActionsTypes.SET_EDIT_MODAL,
  payload,
});

export const setLikeAction = (payload: string): ChatActions => ({
  type: ChatActionsTypes.SET_LIKE,
  payload,
});

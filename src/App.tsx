import React from 'react';
import { Provider } from 'react-redux';
import { Chat } from './components';
import { URL } from './config';
import { store } from './store';

import './styles/App.css';

function App() {
  return (
    <Provider store={store}>
      <Chat url={URL} />
    </Provider>
  );
}

export default App;

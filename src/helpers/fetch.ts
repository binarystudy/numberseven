import { Message } from "../types";

interface IFetchCustomer {
	(url: string): Promise<Message[]>;
}

export const fetchCustomer: IFetchCustomer = async (url) => {
	const response = await fetch(url);
	if (!response.ok) {
		 return [];
	}
	return await response.json();
};

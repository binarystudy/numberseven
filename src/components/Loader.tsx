import React from 'react';
import '../styles/loader.css';

export const Loader: React.FC = () => {
  return (
    <div className="wrapper-preloader">
      <div className="preloader"></div>
    </div>
  );
};

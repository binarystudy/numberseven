import React from 'react';
import { setDividerDate } from '../helpers';
import { Divider } from './Divider';
import { OwnMessage } from './OwnMessage ';
import { Message } from './Message';
import { Message as MessageType, User } from '../types';

interface Props {
  messages: MessageType[];
  user: User;
  onEdit: (id: string) => void;
  onDelete: (id: string) => void;
  onLiked: (id: string) => void;
}

export class MessageList extends React.Component<Props> {
  listsWithDivider() {
    const messages = this.props.messages;
    const newMessages: Map<string, MessageType[]> = new Map();
    messages.forEach((message) => {
      const dividerDate = setDividerDate(message.createdAt);
      newMessages.set(dividerDate, [...(newMessages.get(dividerDate) || []), message]);
    });
    return newMessages;
  }

  render() {
    const messagesMap = this.listsWithDivider();
    const { userId } = this.props.user;
    const { onDelete, onEdit, onLiked } = this.props;
    return (
      <div className="message-list">
        {Array.from(messagesMap).map(([divider, messages], index) => {
          return (
            <div key={index + 1}>
              <Divider text={divider} />
              {messages.map((message) => {
                return userId === message.userId ? (
                  <OwnMessage
                    key={message.id}
                    message={message}
                    onDelete={() => onDelete(message.id)}
                    onEdit={() => onEdit(message.id)}
                  />
                ) : (
                  <Message key={message.id} message={message} onLiked={() => onLiked(message.id)} />
                );
              })}
            </div>
          );
        })}
      </div>
    );
  }
}

import React, { useCallback, useEffect, useState } from 'react';
import { Header } from './Header';
import { Body } from './Body';
import { Footer } from './Footer';

import '../../styles/modal.css';
import { changeClass } from '../../helpers/dom';

const body = document.body;

const setBodyClassName = (type: 'add' | 'remove') => {
  let classes = 'modal-show';
  if (body.scrollHeight > window.innerHeight) {
    classes = `scroll-hide ${classes}`;
  }

  changeClass(body, type, classes);
};

interface Props {
  text: string;
  onClose: () => void;
  onSave: (text: string) => void;
}

export const Modal: React.FC<Props> = ({ text, onClose, onSave }) => {
  const [value, setValue] = useState<string>(text);
  const [disabled, setDisabled] = useState<boolean>(true);

  useEffect(() => {
    setBodyClassName('add');
  }, []);

  const handleCancel = useCallback(() => {
    setBodyClassName('remove');
    onClose();
  }, [onClose]);

  const handleChange: React.ChangeEventHandler<HTMLTextAreaElement> = (event) => {
    const { value } = event.target;
    setDisabled(value === text || !value.length);
    setValue(value);
  };

  const handleSave = useCallback(() => {
    if (!value.length && value === text) {
      return;
    }
    onSave(value);
    setBodyClassName('remove');
  }, [value, text, onSave]);

  const content = <textarea value={value} onChange={handleChange} className="edit-message-input" />;
  return (
    <div className="modal-layer edit-message-modal modal-shown">
      <div className="modal-root">
        <div className="modal-content">
          <Header onClose={handleCancel} title="Edit Message" />
          <Body content={content} />
          <Footer disabled={disabled} onCancel={handleCancel} onOk={handleSave} />
        </div>
      </div>
    </div>
  );
};

import React from 'react';

interface Props {
  title: string;
  onClose: () => void;
}

export const Header: React.FC<Props> = ({ title, onClose }) => {
  return (
    <div className="modal-header">
      <span>{title}</span>
      <i className="fas fa-times close-btn" onClick={() => onClose()}></i>
    </div>
  );
};

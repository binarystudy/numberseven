import React from 'react';

interface Props {
  onOk: () => void;
  onCancel: () => void;
  disabled: boolean;
}

export const Footer: React.FC<Props> = ({ onOk, onCancel, disabled }) => {
  return (
    <div className="modal-footer">
      <button
        disabled={disabled}
        onClick={() => onOk()}
        className={`btn edit-message-button${!disabled ? ' btn-success' : ''}`}
      >
        Ok
      </button>
      <button className="btn edit-message-close" onClick={() => onCancel()}>
        Cancel
      </button>
    </div>
  );
};

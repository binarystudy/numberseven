import React from 'react';

interface Props {
  content: JSX.Element;
}

export const Body: React.FC<Props> = ({ content }) => {
  return <div className="modal-body">{content}</div>;
};

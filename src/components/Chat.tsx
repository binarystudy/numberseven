import React, { useEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Header } from './Header';
import { MessageList } from './MessageList ';
import { MessageInput } from './MessageInput ';
import { Loader } from './Loader';
import { User } from '../types';
import { Modal } from './modal';
import {
  addMessageAction,
  addMessagesAction,
  deleteMessageAction,
  editMessageAction,
  RootState,
  setEditModalAction,
  setLikeAction,
  setMessageForEdit,
  setPreloaderAction,
} from '../store';
import { getUsersCount, createId, fetchCustomer } from '../helpers';

import '../styles/chat.css';
import '../styles/buttons.css';

interface Props {
  url: string;
}

const user: User = { user: 'Test', userId: createId() };

export const Chat: React.FC<Props> = ({ url }) => {
  const { preloader, messages, editModal, message } = useSelector((state: RootState) => state.chat);
  const dispatch = useDispatch();
  useEffect(() => {
    const wrapperFetch = async () => {
      const messages = await fetchCustomer(url);
      dispatch(addMessagesAction(messages));
      dispatch(setPreloaderAction(false));
    };
    wrapperFetch();
  }, [url, dispatch]);

  const handleDelete = useCallback(
    (id: string) => {
      dispatch(deleteMessageAction(id));
    },
    [dispatch]
  );

  const handleEdit = useCallback(
    (id: string) => {
      const index = messages.findIndex((message) => message.id === id);
      if (index !== -1) {
        dispatch(setMessageForEdit(messages[index]));
        dispatch(setEditModalAction(true));
      }
    },
    [messages, dispatch]
  );

  const handleLiked = useCallback(
    (id: string) => {
      dispatch(setLikeAction(id));
    },
    [dispatch]
  );

  const handleSave = useCallback(
    (text: string) => {
      if (!text || !text.length) {
        return;
      }
      dispatch(
        addMessageAction({
          id: createId(),
          ...user,
          avatar: '',
          text,
          createdAt: new Date().toISOString(),
          editedAt: '',
        })
      );
    },
    [dispatch]
  );

  const handleSaveEdited = useCallback(
    (text: string) => {
      if (!message || !text || !text.length) {
        return;
      }
      dispatch(
        editMessageAction({
          ...message,
          text,
          editedAt: new Date().toISOString(),
        })
      );
      dispatch(setMessageForEdit(null));
      dispatch(setEditModalAction(false));
    },
    [message, dispatch]
  );

  const handleCloseModal = () => {
    dispatch(setMessageForEdit(null));
    dispatch(setEditModalAction(false));
  };

  return preloader ? (
    <Loader />
  ) : (
    <div className="chat">
      <Header
        chatName="MyChat"
        messagesCount={messages.length}
        usersCount={getUsersCount(messages)}
        lastMessageDate={messages.length ? messages[messages.length - 1].createdAt : ''}
      />
      <MessageList
        messages={messages}
        onDelete={handleDelete}
        onEdit={handleEdit}
        onLiked={handleLiked}
        user={user}
      />
      <MessageInput onSend={handleSave} />
      {editModal && message && (
        <Modal onSave={handleSaveEdited} text={message.text} onClose={handleCloseModal} />
      )}
    </div>
  );
};

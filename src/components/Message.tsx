import React from 'react';
import { Avatar } from './Avatar';
import { Message as MessageType } from '../types';
import { intlDate } from '../helpers';

import '../styles/message.css';

interface Props {
  message: MessageType;
  onLiked: () => void;
}

export const Message: React.FC<Props> = (props) => {
  const { message, onLiked } = props;
  const { dateMessage, fullDate } = intlDate;
  return (
    <div className="message">
      <Avatar src={message.avatar} alt={message.user} />
      <div className="message-content">
        <div className="message-title">
          <div>
            <span className="message-user-name">{message.user}</span>
            <span className="message-time">{dateMessage(message.createdAt)}</span>
          </div>
          {message.editedAt.length ? (
            <span className="message-edit-time">edit: {fullDate(message.editedAt)}</span>
          ) : null}
        </div>
        <span className="message-text">{message.text}</span>
        <i
          className={`${message.liked ? 'message-liked fas' : 'message-like far'} fa-thumbs-up`}
          onClick={() => onLiked()}
        ></i>
      </div>
    </div>
  );
};

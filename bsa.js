import { Chat } from './src/components';
import { rootReducer } from './src/store';

export default { Chat, rootReducer };
